import csv
from airflow.utils.edgemodifier import Label
from datetime import datetime, timedelta
from textwrap import dedent
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from airflow import DAG
from airflow.models import Variable
import sqlite3
import csv

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

## Do not change the code below this line ---------------------!!#

def read():
# conectando...
    conn = sqlite3.connect('/data/Northwind_small.sqlite')
    cursor = conn.cursor()
# lendo os dados
    cursor.execute("""
    SELECT * FROM Order;
    """)
# criando tabela
    with csv.open('output_orders.csv', 'w') as f:
        for linha in conn.iterdump():
            f.write('%s\n' % linha)
    conn.close()


def read_od():
# conectando...
    conn = sqlite3.connect('/data/Northwind_small.sqlite')
    cursor = conn.cursor()
# task...
    cursor.execute("""with cte AS(
    SELECT * FROM OrderDetail;
    JOIN output_orders.csv;)
    """)
    conn.close()

def sum():
# conectando...
    conn = sqlite3.connect('/data/Northwind_small.sqlite')
    cursor = conn.cursor()
# task...
    text_soma = cursor.execute("""SELECT SUM Quantity FROM cte;
    WHERE Shipcity = Rio de Janeiro""")
# criando tabela
    arquivo = open('output.txt', 'w')
    arquivo.write(str(text_soma))
    conn.close()


def export_final_answer():
    import base64

    # Import count
    with open('count.txt') as f:
        count = f.readlines()[1]

    my_email = Variable.get("my_email")
    message = my_email+count
    message_bytes = message.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes)
    base64_message = base64_bytes.decode('ascii')

    with open("final_output.txt","w") as f:
        f.write(base64_message)
    return f


## Do not change the code above this line-----------------------##

with DAG(
    'Desafio_exec',
    default_args=default_args,
    description='Desafio de Airflow da Indicium',
    schedule_interval=timedelta(days=1),
    start_date=datetime(2021, 1, 1),
    catchup=False,
    tags=['desafio'],
) as dag:
    dag.doc_md = """
        Esse é o desafio de Airflow da Indicium.
    """
   
    task_order = PythonOperator(
        task_id='read_order',
        python_callable=read,
         provide_context=True
    )

    task_order.doc_md = """
        Essa task le os dados da tabela order e cria um file .csv
    """

    task_order_detail = PythonOperator(
        task_id='order_detail',
        python_callable=read_od,
        op_kwargs=sum
    )
    
    task_order_detail.doc_md = """
        Essa task le os dados da tabela OrderDetail faz um Join no antigo arquivo CSV e soma a quantidade de produtos que tem Shipcity = Rio de Janeiro
    """

    export_final_output = PythonOperator(
        task_id='export_final_output',
        python_callable=export_final_answer,
        provide_context=True
    )

task_order >> task_order_detail >> export_final_output

my_email = 'bruno.bennertz@indicium.tech'
