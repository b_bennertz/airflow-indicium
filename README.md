# O Airflow

do site do próprio airflow:

    Airflow is a platform created by the community to programmatically author, schedule and monitor workflows.


## Primeiras Impressões

Para ter uma ideia do que se trata a ferramenta, primeiro vamos executar o Airflow localmente com os DAGS(Directed acyclic graphs, o conceito usado para implementar pipelines).

para instalar o airflow vamos primeiro criar um virtualenv e depois rodar o script install.sh. Esse script é um ctrl c ctrl v das instruções encontradas no site.

```
virtualenv venv -p python3
source venv/bin/activate
bash install.sh
```
Se as coisas deram certo, no terminal vai aparecer a seguinte mensagem:

```
standalone | 
standalone | Airflow is ready
standalone | Login with username: admin  password: sWFbFYrnYFAfYgY3
standalone | Airflow Standalone is for development purposes only. Do not use this in production!
standalone |
```

airflow roda na porta 8080, então podemos acessar em 
http://localhost:8080

## Limpando os Dags de Exemplo

Para tirar os dags de exemplo e começar um dag nosso, podemos apagar os arquivos
airflow-data/data e airflow-data/admin-password.txt, e editar o arquivo airflow.cfg trocando:
```
load_examples = True
```
para
```
load_examples = False
```

Feito isso, primeiro precisamos configurar o ambiente para dizer onde vão ficar os arquivos de config do airflow, fazemos isso configurando a seguinte variavel de ambiente:

```
export AIRFLOW_HOME=./airflow-data
```

Dessa forma configuramos o airflow para colocar suas configurações dentro da pasta desse tutorial na pasta /airflow-data

Na sequência rodamos o comando para resetar o db do airflow e fazer start do airflow local:

```
airflow db reset
airflow standalone
```

# A DAG
```
AIRFLOW_HOME/dags
```

Em nosso caso AIRFLOW_HOME é airflow-data, entao criaremos uma pasta dags e um arquivo
elt_dag.py dentro de airflow-data com o seguinte conteudo:

# DAG 1 de 3

```
import csv
from airflow.utils.edgemodifier import Label
from datetime import datetime, timedelta
from textwrap import dedent
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from airflow import DAG
from airflow.models import Variable
import sqlite3
import csv

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}
```
aqui colocamos os imports q a DAG tem que fazer e seus default_args

# DAG 2 de 3

```
def read():
# conectando...
    conn = sqlite3.connect('/data/Northwind_small.sqlite')
    cursor = conn.cursor()
# lendo os dados
    cursor.execute("""
    SELECT * FROM Order;
    """)
# criando tabela
    with csv.open('output_orders.csv', 'w') as f:
        for linha in conn.iterdump():
            f.write('%s\n' % linha)
    conn.close()


def read_od():
# conectando...
    conn = sqlite3.connect('/data/Northwind_small.sqlite')
    cursor = conn.cursor()
# task...
    cursor.execute("""with cte AS(
    SELECT * FROM OrderDetail;
    JOIN output_orders.csv;)
    """)
    conn.close()

def sum():
# conectando...
    conn = sqlite3.connect('/data/Northwind_small.sqlite')
    cursor = conn.cursor()
# task...
    text_soma = cursor.execute("""SELECT SUM Quantity FROM cte;
    WHERE Shipcity = Rio de Janeiro""")
# criando tabela
    arquivo = open('output.txt', 'w')
    arquivo.write(str(text_soma))
    conn.close()


def export_final_answer():
    import base64

    # Import count
    with open('count.txt') as f:
        count = f.readlines()[1]

    my_email = Variable.get("my_email")
    message = my_email+count
    message_bytes = message.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes)
    base64_message = base64_bytes.decode('ascii')

    with open("final_output.txt","w") as f:
        f.write(base64_message)
    return f


## Do not change the code above this line-----------------------##
```

Aqui tem as funcoes que serao utilizadas no pythonOperator

# DAG 3 de 3

'''
with DAG(
    'Desafio_exec',
    default_args=default_args,
    description='Desafio de Airflow da Indicium',
    schedule_interval=timedelta(days=1),
    start_date=datetime(2021, 1, 1),
    catchup=False,
    tags=['desafio'],
) as dag:
    dag.doc_md = """
        Esse é o desafio de Airflow da Indicium.
    """
   
    task_order = PythonOperator(
        task_id='read_order',
        python_callable=read,
         provide_context=True
    )

    task_order.doc_md = """
        Essa task le os dados da tabela order e cria um file .csv
    """

    task_order_detail = PythonOperator(
        task_id='order_detail',
        python_callable=read_od,
        op_kwargs=sum
    )
    
    task_order_detail.doc_md = """
        Essa task le os dados da tabela OrderDetail faz um Join no antigo arquivo CSV e soma a quantidade de produtos que tem Shipcity = Rio de Janeiro
    """

    export_final_output = PythonOperator(
        task_id='export_final_output',
        python_callable=export_final_answer,
        provide_context=True
    )

task_order >> task_order_detail >> export_final_output

my_email = 'bruno.bennertz@indicium.tech'
'''

aqui demos o nome a DAG e suas tarefas juntamente com a ordem de execucao e a variavel de email